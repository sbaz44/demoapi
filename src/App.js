import React, { Component } from 'react';
import './App.css'

class App extends Component {
  state = { 
    Username:"",
    Password:""
   }
  handleChange1 = event => {
    this.setState({
      Username: event.target.value
    });
  };

  handleChange2 = event => {
    this.setState({
      Password: event.target.value
    });
  };

  render() { 
    const getData=()=>{
      return alert(this.state.Password)
      //API CALL HERE
    }
    return ( 
<div className="App">
      
      <div className="container">
          <div className='header'> Login </div>
          <div className="inputContainer">
          <input
          type="text"
          placeholder="Username"
          value={this.props.value}
          onChange={this.handleChange1}
        />
        <input
          type="password"
          placeholder="Password"
          onChange={this.handleChange2}
        />
            </div>
            <button className="btn" onClick={()=>getData(this.state.Username,this.state.Password)}>Login</button>
         
      </div>
     

          
    </div>

     );
  }
}
 
export default App;